from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

app = FastAPI()

@app.get("/info")
def info():
    latency_data = {}
    with open("./latency/octaplus") as f:
        latency_data['octaplus'] = f.read()
    with open("./latency/backup") as f:
        latency_data['backup'] = f.read()
    with open("./latency/active") as f:
        active = f.read()
    with open("../failover.log") as f:
        failover = f.read()
    return {
        "latency": latency_data,
        "active": active,
        "log": failover
    }

app.mount("/", StaticFiles(directory="../ui", html=True))
