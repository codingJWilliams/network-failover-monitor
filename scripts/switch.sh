cd /root/network-failover-monitor/scripts

log() {
    echo "$(date --rfc-3339=seconds) | SWITCH | $1" >> /root/network-failover-monitor/failover.log
}

switch() {
    ssh -i ~/.ssh/id_mikrotik_admin admin@10.0.101.1 "/ip/route/disable [find comment=\"$1\"]"
    ssh -i ~/.ssh/id_mikrotik_admin admin@10.0.101.1 "/ip/route/enable [find comment=\"$2\"]"
}

is_enabled() {
    out=$(ssh -i ~/.ssh/id_mikrotik_admin admin@10.0.101.1 "/ip/route/print where comment=\"$1\"" | grep "As")
    if [[ -z "$out" ]]; then
        return 1
    else
        return 0
    fi
}



if [[ "$(cat ../src/latency/octaplus)" == "Error" ]] && ! is_enabled 4g_wan; then
    log "Octaplus potentially erorred, double checking"
    sleep 10
    ip netns exec octaplus ping -c 2 1.1.1.1
    if [[ "$?" != "0" ]]; then
        log "Octaplus down. Switching to 4G"
        # Octaplus is down
        switch octaplus_wan 4g_wan
        echo -n "backup" > ../src/latency/active
    else
        log "False alarm, remaining on Octaplus"
        switch 4g_wan octaplus
        echo -n "octaplus" > ../src/latency/active
    fi
elif [[ "$(cat ../src/latency/octaplus)" != "Error" ]] && is_enabled 4g_wan; then
    log "Octaplus seems back online, switching back"
    switch 4g_wan octaplus_wan
    echo -n "octaplus" > ../src/latency/active
# elif ! is_enabled octaplus_wan; then
#     if ! is_enabled 4g_wan; then
#         log "It seems no wan is enabled. Enabling octaplus"
#         switch 4g_wan octaplus_wan
#     fi
fi