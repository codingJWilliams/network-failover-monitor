cd /root/network-failover-monitor/scripts


log() {
    echo "$(date --rfc-3339=seconds) | INFO | $1" >> /root/network-failover-monitor/failover.log
}

calc_avg() {
    average_1="$(ip netns exec $1 ping -c 2 1.1.1.1 | grep "rtt min" | cut -d'/' -f5)"
    average_2="$(ip netns exec $1 ping -c 2 8.8.8.8 | grep "rtt min" | cut -d'/' -f5)"
    if [[ -z "$average_1" ]] || [[ -z "$average_2" ]]; then
        average="Error"
        log "${1}: Ping failed"
    else
        average="$(python3 -c "print(round(($average_1 + $average_2) / 2, 3))")ms"
    fi
    echo -n "$average"
}

octaplus="$(calc_avg octaplus)"
echo -n "$octaplus" > ../src/latency/octaplus
backup="$(calc_avg backup)"
echo -n "$backup" > ../src/latency/backup
if [[ "$octaplus" == "Error" ]] || [[ "$backup" == "Error" ]]; then
    log "Uplink status: octaplus=$octaplus backup=$backup"
fi